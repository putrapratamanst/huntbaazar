@extends('layout.partials.main')
@section('content')

@if($errors->any())
<div class="alert alert-danger">
    @foreach($errors->all() as $err)
    <li><span>{{ $err }}</span></li>
    @endforeach
</div>
@endif
<div class="container-contact100">

    <div class="contact100-more flex-col-c-m" style="background-image: url('{{ asset ('images/bg2.jpg') }}');">
        <span class="txt1 p-b-20">Your Code Registration: {{$hash}} </span>

            <p id="countdowntimer" style="text-align: center;font-size: 40px;margin-top: 0px;color:white"></p>
            <br>
            <div class="flex-w size1 p-b-47">

                <div class="flex-col size2">
                    <span class="txt1 p-b-20">
                        <marquee>Hunt Baazar - 24 Juli 2020. Don't Miss It!</marquee>
                    </span>

                </div>
            </div>

            <div class="dis-flex size1 p-b-47">
                <div class="txt1 p-r-25">
                    <span class="lnr lnr-phone-handset"></span>
                </div>

                <div class="flex-col size2">
                    <span class="txt1 p-b-20">
                        Lets Talk
                    </span>

                    <span class="txt3">
                        +62 813 1920 8888
                    </span>
                </div>
            </div>

            <div class="dis-flex size1 p-b-47">
                <div class="txt1 p-r-25">
                    <span class="lnr lnr-envelope"></span>
                </div>

                <div class="flex-col size2">
                    <span class="txt1 p-b-20">
                        General Support
                    </span>

                    <span class="txt3">
                        huntstreet@example.com
                    </span>
                </div>
            </div>
    </div>
</div>


</div>
@endsection('content')

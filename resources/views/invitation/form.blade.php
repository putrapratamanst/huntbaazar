@extends('layout.partials.main')
@section('content')

@if($errors->any())
<div class="alert alert-danger">
    @foreach($errors->all() as $err)
    <li><span>{{ $err }}</span></li>
    @endforeach
</div>
@endif
<div class="container-contact100">
    <div class="wrap-contact100">
        {{ Form::open(array('url' => 'invitation/create', 'class'=>"contact100-form validate-form")) }}

        <!-- <form action="create" method="POST" class="contact100-form validate-form"> -->
        @csrf
        <span class="contact100-form-title">
            Send Your Invitation!
        </span>

        <label class="label-input100" for="email">Enter your email *</label>
        <div class="wrap-input100" data-validate="You Must Select">
            <input id="email" class="input100" type="text" name="email" value={{ $email }} placeholder="Full Email" readonly>
            <span class="focus-input100"></span>
        </div>



        <label class="label-input100" for="full-name">Tell us your name *</label>
        <div class="wrap-input100 validate-input" data-validate="Type Full name">
            <input id="full-name" class="input100" type="text" name="name" placeholder="Full name">
            <span class="focus-input100"></span>
        </div>



        <label class="label-input100" for="date_of_birth">Enter Date of Birth</label>
        <div class="wrap-input100 validate-input">
            <input id="date_of_birth" class="input100" type="date" name="date_of_birth" placeholder="yyyy-dd-mm" data-validate="Type Date of Birth">
            <span class="focus-input100"></span>
        </div>

        <label class="label-input100" for="gender">Enter Gender</label>
        <div class="wrap-input100">
            {{Form::select('gender', ['1' => "Laki-Laki", '2' => "Perempuan"], '1', ['class' => 'input100' ])}}
            <span class="focus-input100"></span>
        </div>

        <label class="label-input100" for="favorite-designer">Tell us your favorite designer *</label>
        <div class="wrap-input100" data-validate="Please Select Designer">
            {{Form::select('designer', $designerList, 'default', ['class' => 'input100'])}}
        </div>


        <div class="container-contact100-form-btn">
            <button class="contact100-form-btn">
                Send Invitation
            </button>
        </div>
        <!-- </form> -->
        {{ Form::close() }}

        <div class="contact100-more flex-col-c-m" style="background-image: url('{{ asset ('images/bg2.jpg') }}');">
            <p id="countdowntimer" style="text-align: center;font-size: 40px;margin-top: 0px;color:white"></p>
            <br>
            <div class="flex-w size1 p-b-47">

                <div class="flex-col size2">
                    <span class="txt1 p-b-20">
                        <marquee>Hunt Baazar - 24 Juli 2020. Don't Miss It!</marquee>
                    </span>

                </div>
            </div>

            <div class="dis-flex size1 p-b-47">
                <div class="txt1 p-r-25">
                    <span class="lnr lnr-phone-handset"></span>
                </div>

                <div class="flex-col size2">
                    <span class="txt1 p-b-20">
                        Lets Talk
                    </span>

                    <span class="txt3">
                        +62 813 1920 8888
                    </span>
                </div>
            </div>

            <div class="dis-flex size1 p-b-47">
                <div class="txt1 p-r-25">
                    <span class="lnr lnr-envelope"></span>
                </div>

                <div class="flex-col size2">
                    <span class="txt1 p-b-20">
                        General Support
                    </span>

                    <span class="txt3">
                        huntstreet@example.com
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection('content')

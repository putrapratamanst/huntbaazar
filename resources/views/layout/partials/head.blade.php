<head>
    <title>{{ config('app.name') }}
    </title>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{{ asset('css/main.css') }}" rel="stylesheet" type="text/css">

    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="{{ asset('resources/templates/favicons.png') }}" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset ('vendor/bootstrap/css/bootstrap.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset ('fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset ('fonts/Linearicons-Free-v1.0.0/icon-font.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset ('vendor/animate/animate.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset ('vendor/css-hamburgers/hamburgers.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset ('vendor/animsition/css/animsition.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset ('vendor/select2/select2.min.css') }}">
    
    <link rel="stylesheet" type="text/css" href="{{ asset ('css/list.css') }}">
    <!--===============================================================================================-->
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset ('css/util.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset ('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset ('css/jquery-ui.css') }}">

    <!--===============================================================================================-->
</head>

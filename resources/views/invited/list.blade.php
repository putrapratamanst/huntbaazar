@extends('layout.partials.main')
@section('content')
<style>
    .container-contact100 {
        /* width: 100%; */
        min-height: 60vh;
        display: -webkit-box;
        display: -webkit-flex;
        display: -moz-box;
        display: -ms-flexbox;
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
        align-items: center;
        padding: 15px;
        background: #f2f2f2;
    }
</style>

<ul>
    <li><a class="active pull-right" href="#home">LOGOUT</a></li>
    <li><a class="" href="/invited/form">CREATE INVITED</a></li>
</ul>

@if(\Session::has('alert'))
<div class="alert alert-danger">
    <div>{{Session::get('alert')}}</div>
</div>
@endif
<div class="container-contact100">

    <br>
    <h1><u>Invited List</u></h1>
    <br>


    <table>
        <tr>
            <th>ID</th>
            <th>Email</th>
            <th>Status</th>
            <th>Generate Link</th>
        </tr>
        @foreach ($list as $lists)
        <tr>
            <td>{{ $lists->id }}</td>
            <td>{{ $lists->email }}</td>
            <td>{{ $lists->is_invited ? "Registered" : "Unregistered" }}</td>
            @if($lists['links']['link'])
            <td><a href={{ $lists['links']['link'] }}>{{ $lists['links']['link'] }}</td>
            @else
            <td><a href={{url('admin/generate-link/'.$lists->id)}}>Generate Link</td>
            @endif
        </tr>
        @endforeach
    </table>
</div>
@endsection('content')

@extends('layout.partials.main')
@section('content')

@if($errors->any())
<div class="alert alert-danger">
    @foreach($errors->all() as $err)
    <li><span>{{ $err }}</span></li>
    @endforeach
</div>
@endif
<div class="container-contact100">
    {{ Form::open(array('url' => 'invited/create', 'class'=>"contact100-form validate-form")) }}

    <!-- <form action="create" method="POST" class="contact100-form validate-form"> -->
    @csrf
    <span class="contact100-form-title">
        Input Email Invited
    </span>


    <label class="label-input100" for="full-name">Email *</label>
    <div class="wrap-input100 validate-input" data-validate="Type Email">
        <input id="email" class="input100" type="text" name="email">
        <span class="focus-input100"></span>
    </div>
    <br>

    <div class="col-md-6">
        <a href="/admin/list" class="contact100-form-btn btn-primary pull-left"> Back </a> <br>
    </div>
    <div class="col-md-6">
        <button class="contact100-form-btn">
            Send Invitation
        </button>
    </div>
        <!-- </form> -->
        {{ Form::close() }}

    </div>
    @endsection('content')

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    protected $table = 'invitations';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    // protected $casts = [
    //     'company_id'  => 'integer',
    //     'input_type'  => 'integer',
    //     'input_value' => 'integer',
    //     'state'       => 'integer',
    //     'created_by'  => 'integer',
    //     'updated_by'  => 'integer',
    // ];

    public static $rules = [
        'onCreate' => [
            'email'         => 'required|string',
            'name'          => 'required|string',
            'date_of_birth' => 'required|string',
            'designer'      => 'required|string',
            'gender'        => 'required|string',
        ],
    ];

    protected $hidden = [];

    // public function boardStage()
    // {
    //     return $this->hasMany('App\Models\BoardStage', 'board_id', 'id');
    // }

    // public function boardUserMapping()
    // {
    //     return $this->hasMany('App\Models\BoardUserMapping', 'board_id', 'id');
    // }
}

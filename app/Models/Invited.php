<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invited extends Model
{
    protected $table = 'invited';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public function links()
    {
        return $this->hasOne('App\Models\InvitedLink','email', 'email');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvitedLink extends Model
{
    protected $table = 'invited_link';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public function user()
    {
        return $this->belongsTo('App\Models\Invited');
    }

    public function statusInfo()
    {
        return $this->hasOne('App\Models\Status', 'id', 'status');
    }

}

<?php

namespace App\Traits;

use App\Models\Invited;
use App\Models\InvitedLink;

trait InvitedLinkTrait
{
    public static function createLinkByEmail($email, $url)
    {
        $model          = new InvitedLink();
        $model->email   = $email;
        $model->link    = $url;
        $model->status  = true;
        $model->save();
    }

    public static function getDataByEmail($email)
    {
        return  InvitedLink::where('email', $email)
            ->where('status', true)
            ->whereNull('deleted_at')
            ->first();
    }
}

<?php

namespace App\Traits;

use App\Mail\SendEmail;
use App\Mail\SendEmailRegister;
use App\Models\Invitation;
use Illuminate\Support\Facades\Mail;

trait InvitationTrait
{
    public static function createDataInvitation($params)
    {
        $model                    = new Invitation();
        $model->invited_id        = InvitedTrait::getEmail($params['email'])['id'];
        $model->date_of_birth     = $params['date_of_birth'];
        $model->gender            = $params['gender'];
        $model->favorite_designer = $params['designer'];
        $model->save();

        $dataInvited = InvitedTrait::getEmail($params['email']);
        $dataInvited->is_invited = true;
        $dataInvited->save();

        return $dataInvited;
    }

    public static function sendInvitation($email, $url)
    {
        Mail::to($email)->send(new SendEmail($email, $url));
    }

    public static function sendInvitationRegistered($email)
    {
        Mail::to($email)->send(new SendEmailRegister($email));
    }

    public static function getInvitationByInvitedId($id)
    {
        return Invitation::where('invited_id', $id)
            ->whereNull('deleted_at')
            ->first();

    }

}

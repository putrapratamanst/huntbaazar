<?php

namespace App\Traits;

use App\Models\Invited;

trait InvitedTrait
{
    public static function getListEmail(array $selectedField)
    {
        return Invited::select($selectedField)
            ->where('is_invited', false)
            ->whereNull('deleted_at')
            ->get();
    }

    public static function reformatDropdownListEmail($data)
    {
        $callback = [
            'default' => "Please select one email"
        ];

        if (count($data) == 0) {
            return $callback;
        }

        $collection = collect($data)->map(function ($item) {
            $newData = [
                $item->email => $item->email
            ];
            return $newData;
        });


        $callback = $collection->prepend($callback)->flatMap(function ($item) {
            return $item;
        });
        return $callback;
    }

    public static function getEmail($email)
    {
        $data = Invited::where('email', $email)
            ->whereNull('deleted_at')
            ->first();

        if (!$data)
            return 0;

        return $data;
    }

    public static function getById($id)
    {
        $data = Invited::where('id', $id)
            ->whereNull('deleted_at')
            ->first();

        return $data;
    }

    public static function getListInvited()
    {
        return Invited::with(['links','links.statusInfo'])->get();
    }

    public static function createDataInvited($model, $params)
    {
        $model->email      = $params['email'];
        $model->is_invited = false;
        $model->save();
    }
}

<?php 

namespace App\Http\Helpers;

use App\Http\Helpers\Curl;

class ScrappingHelper {

    public static function scrap($url)
    {
        $result = Curl::exec($url);

        preg_match_all('!<a href="https:\/\/www\.huntstreet\.com\/shop\?designer=.*?">(.*?)<\/a>!', $result, $match);

        $designer = $match[1]; 
        $callback = [];
        if(count($designer) > 0){
            foreach ($designer as $key => $value) {

                $callback[$value] = $value;
            }
        }

        return $callback;
    }
}

<?php

namespace App\Http\Helpers;

class Curl
{
    public static function exec($url)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($curl);

        return $result;
    }
}

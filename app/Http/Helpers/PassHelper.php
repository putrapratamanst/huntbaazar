<?php


namespace App\Http\Helpers;


class PassHelper
{
    public static function encrypt($data)
    {
        $ciphering = "AES-128-CTR";
        $iv_length = openssl_cipher_iv_length($ciphering);
        $options = 0;

        $encryption_iv = '1234567891011121';

        $encryption_key = env('SECRET_KEY');

        $encryption = openssl_encrypt(
            $data,
            $ciphering,
            $encryption_key,
            $options,
            $encryption_iv
        );
        return $encryption;
  
    }

    public static  function decrypt($data)
    {
        $ciphering = "AES-128-CTR";
        $options = 0;

        $decryption_iv = '1234567891011121';

        $decryption_key = env('SECRET_KEY');
        $decryption = openssl_decrypt(
            $data,
            $ciphering,
            $decryption_key,
            $options,
            $decryption_iv
        );
        
        return $decryption;
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Helpers\PassHelper;
use App\Http\Requests\InvitationRequest;
use App\Models\Invitation;
use App\Traits\InvitationTrait;
use App\Traits\InvitedTrait;
use Exception;
use App\Http\Helpers\ScrappingHelper;
use App\Http\Requests\AdminRequest;
use App\Models\Admin;
use App\Models\Invited;
use App\Traits\InvitedLinkTrait;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    use InvitedTrait, InvitedLinkTrait, InvitationTrait, AuthenticatesUsers;

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->middleware('guest')->except('logout');

    }
    public function index()
    {
        $listInvited = $this->getListInvited();

        return view('invited/list')->with([
            'list' => $listInvited
        ]);
        
    }

    public function login()
    {
        return view('login');
    }

    public function loginPost(Request $request)
    {
        $email = $request->email;
        $password = $request->password;

        $data = Admin::where('email', $email)->first();
        if ($data) {
            if (Hash::check($password, $data->password)) {
                Session::put('name', $data->name);
                Session::put('email', $data->email);
                Session::put('login', TRUE);
                return redirect('admin');
            } else {
                return redirect('admin/login')->with('alert', 'Password atau Email, Salah !');
            }
        } else {
            return redirect('admin/login')->with('alert', 'Password atau Email, Salah!');
        }
    }

    public function logout()
    {
        Session::flush();
        return redirect('admin/login')->with('alert', 'Kamu sudah logout');
    }

    public function generateLink($invitedId)
    {
        $dataInvited = $this->getById($invitedId);

        if($dataInvited == false)
            return redirect('admin')->with('alert', 'Data Invited Not Found !');
        
        $encrypt = PassHelper::encrypt($dataInvited->id);
        $url     = env('BASE_URL')."invitation/".$encrypt;

        $dataInvited->is_invited = true;
        $dataInvited->save();

        $this->createLinkByEmail($dataInvited->email, $url);

        $this->sendInvitation($dataInvited->email, $url);
        return redirect('admin')->with('alert', 'Link Has Been Sent To User');
    }
}

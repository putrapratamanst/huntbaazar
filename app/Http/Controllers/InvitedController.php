<?php

namespace App\Http\Controllers;

use App\Models\Invitation;
use App\Traits\InvitationTrait;
use App\Traits\InvitedTrait;
use Exception;
use App\Http\Requests\InvitedRequest;
use App\Models\Invited;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class InvitedController extends Controller
{
    use InvitedTrait;

    protected $request;
    protected $params;

    function __construct(Request $request)
    {
        $this->request = $request;
        $this->params = $request->all();
    }

    public function form()
    {
        return view('invited/form');
    }
    public function create()
    {
        $model = new Invited();
        // $validator = Validator::make($this->params, Invitation::$rules['onCreate']); //todo validation duplicate email
        // if ($validator->fails()) {
        //     throw new Exception(implode(' | ', $validator->errors()->all()));
        // }

       $this->createDataInvited($model, $this->params);
        return redirect('admin');
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Helpers\PassHelper;
use App\Models\Invitation;
use App\Traits\InvitationTrait;
use App\Traits\InvitedTrait;
use Exception;
use App\Http\Helpers\ScrappingHelper;
use App\Http\Requests\InvitationRequest;
use App\Jobs\SendEmail;
use App\Traits\InvitedLinkTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class InvitationController extends Controller
{
    use InvitationTrait, InvitedTrait, InvitedLinkTrait;

    protected $request;
    protected $params;

    // function __construct(InvitationRequest $request)
    function __construct(Request $request)
    {
        $this->request = $request;
        $this->params = $request->all();
    }

    public function form($invitationId)
    {
        $decrypt      = PassHelper::decrypt($invitationId);
        $invited      = $this->getById($decrypt);
        if(!$invited)
            return abort(404);

        if($invited->is_invited == 1){
            $dataInvitation = $this->getInvitationByInvitedId($invited->id);
            if($dataInvitation)
            {
                $hash = $invited->email.":".$invited->id;
                $hashEncrypt = PassHelper::encrypt($hash);
                return view('invitation/detail')->with(['hash' => $hashEncrypt]);
            }
        }

        $checkLink = $this->getDataByEmail($invited->email);
        if (!$checkLink)
            return abort(404);

        $designerList = ScrappingHelper::scrap(env('HUNTSTREET_DESIGNER_URI'));
        $designerList = collect($designerList)->prepend('Please Select Designer','default');

        return view('invitation/form', [
            'email'        => $invited->email,
            'designerList' => $designerList
        ]);
    }

    public function create()
    {
        $validator = Validator::make($this->params, Invitation::$rules['onCreate']);
        if ($validator->fails()) {
            throw new Exception(implode(' | ', $validator->errors()->all()));
        }

        $create = $this->createDataInvitation($this->params);
        dispatch(new SendEmail($this->params['email']));
        $hash = $this->params['email'] . ":" . $create->id;
        $hashEncrypt = PassHelper::encrypt($hash);
        return view('invitation/detail')->with(['hash' => $hashEncrypt]);
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InvitationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'invitation_id'     => 'required|integer',
            'date_of_birth'     => 'required|string',
            'favorite_designer' => 'required|string',
            'name'              => 'required|string',
        ];
    }

    public static $messages = [
        'required' => 'The :attribute field is required.',
        'integer'  => 'The :attribute field must be an integer.',
        'string'   => 'The :attribute field must be string.',
    ];
}

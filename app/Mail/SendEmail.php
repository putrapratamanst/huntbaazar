<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $invitation;
    public $link;
    public function __construct($invitation, $link)
    {
        $this->invitation = $invitation;
        $this->link = $link;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    // public function build()
    // {
    //     return $this->view('view.name');
    // }

    public function build()
    {
        return $this->from(env('MAIL_USERNAME'))
            ->view('email_template')->with([
                'email' => $this->invitation,
                'link'   => $this->link
            ]);
    }
}

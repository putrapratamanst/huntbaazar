<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // insert data ke table pegawai menggunakan Faker
        DB::table('status')->insert([
            'info'       => "Registered",
            'created_at' => Carbon::now()
        ]);
        DB::table('status')->insert([
            'info'       => "Unregistered",
            'created_at' => Carbon::now()
        ]);
    }
}

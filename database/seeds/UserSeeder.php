<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // insert data ke table pegawai menggunakan Faker
        DB::table('status')->insert([
            'name'       => "admin",
            'email'      => "admin@admin.com",
            "password"   => '$2b$10$EgI5vVsBzsOO5lKHewQyF.gIMpaoA4pNzAMPNRmZdAdQa/wePZ.lG'
        ]);
    }
}

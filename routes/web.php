<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('form', 'InvitedController@form');
// Route::post('create', 'InvitedController@create');
// // Route::get('/', function () {
// //     return view('welcome');
// // });

// use Illuminate\Support\Facades\Route;


$router->group(['prefix' => 'invitation'], function () use ($router) {
    $router->post('create', 'InvitationController@create');
    $router->get('{invitationId}', 'InvitationController@form');
});

$router->group(['prefix' => 'invited'], function () use ($router) {
    $router->get('form', 'InvitedController@form');
    $router->post('create', 'InvitedController@create');
});

$router->group(['prefix' => 'admin'], function () use ($router) {
    $router->get('', 'AdminController@index');
    $router->get('list', 'AdminController@list');
    $router->post('create', 'InvitationController@create');
    $router->get('login', 'AdminController@login');
    $router->post('loginPost', 'AdminController@loginPost');
    $router->get('logout', 'AdminController@logout');
    $router->get('generate-link/{invited_id}', 'AdminController@generateLink');
});
